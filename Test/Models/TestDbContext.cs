﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;

namespace Test.Models
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {

        }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Post);

            modelBuilder.Entity<Post>()
                .UsePropertyAccessMode(PropertyAccessMode.Property);

            modelBuilder.Entity<Comment>()
                .Navigation(c => c.Post)
                .UsePropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}
