﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [MaxLength(2500)]
        public string Description { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        public DateTime Date { get; set; }
    }
}
