﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class Post
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }

        [MaxLength(2500)]
        public string Description { get; set; }

        public List<Comment> Comments { get; set;}

    }
}
