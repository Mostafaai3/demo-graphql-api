﻿using GraphQL.Types;

namespace Test.GraphQL.Types
{
    public class PostInputType: InputObjectGraphType
    {
        public PostInputType()
        {
            Field<StringGraphType>("title");
            Field<StringGraphType>("description");
        }
    }
}
