﻿using GraphQL;
using GraphQL.Types;
using Test.Models;
using Test.Repositories;

namespace Test.GraphQL.Types
{
    public class PostType: ObjectGraphType<Post>
    {
        public PostType(ICommentRepository commentRepository)
        {
            Field(p => p.Id);
            Field(p => p.Title);
            Field(p => p.Description);
            Field<ListGraphType<CommentType>>(
                "comments",
                arguments: new QueryArguments(new QueryArgument<IntGraphType>{ Name = "take"}),
                resolve: context => commentRepository.GetCommentsByPostId(context.Source.Id, context.GetArgument<int>("take"))
                );
        }
    }
}
