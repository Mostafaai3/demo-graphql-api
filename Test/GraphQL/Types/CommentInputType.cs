﻿using GraphQL.Types;

namespace Test.GraphQL.Types
{
    public class CommentInputType: InputObjectGraphType
    {
        public CommentInputType()
        {
            Field<StringGraphType>("description");
            Field<IntGraphType>("postId");
        }
    }
}
