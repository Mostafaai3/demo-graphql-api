﻿using GraphQL.Types;
using Test.Models;

namespace Test.GraphQL.Types
{
    public class CommentType: ObjectGraphType<Comment>
    {
        public CommentType()
        {
            Field(c => c.Id);
            Field(c => c.Description);
            Field(c => c.PostId);
            Field(c => c.Date);
        }
    }
}
