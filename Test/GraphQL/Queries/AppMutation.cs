﻿using GraphQL;
using GraphQL.Types;
using Microsoft.EntityFrameworkCore.Storage;
using Test.GraphQL.Types;
using Test.Models;
using Test.Repositories;

namespace Test.GraphQL.Queries
{
    public class AppMutation: ObjectGraphType
    {
        public AppMutation(IPostRepository postRepository, ICommentRepository commentRepository)
        {
            Field<PostType>(
                "createPost",
                arguments: new QueryArguments(new QueryArgument<PostInputType> { Name = "post" }),
                resolve: context =>
                {
                    var post = context.GetArgument<Post>("post");
                    return postRepository.AddPost(post);
                });
            Field<PostType>(
                "updatePost",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }, new QueryArgument<PostInputType> { Name = "post" }),
                resolve: context =>
                {
                    var oldPost = context.GetArgument<Post>("post");
                    var post = postRepository.GetPostById(context.GetArgument<int>("id"));
                    post.Title = oldPost.Title;
                    post.Description = oldPost.Description;
                    return postRepository.UpdatePost(post);
                });
            Field<StringGraphType>(
                "deletePost",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context =>
                {
                    var post = postRepository.GetPostById(context.GetArgument<int>("id"));
                    if (post == null)
                    {
                        context.Errors.Add(new ExecutionError("Post not found"));
                        return null;
                    }
                    postRepository.DeletePost(post);
                    return "Post deleted successfully";
                });
            Field<CommentType>(
                "createComment",
                arguments: new QueryArguments(new QueryArgument<CommentInputType> { Name = "comment" }),
                resolve: context =>
                {
                    var comment = context.GetArgument<Comment>("comment");
                    return commentRepository.AddComment(comment);
                });
            Field<StringGraphType>(
                "deleteComment",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context =>
                {
                    var comment = commentRepository.GetCommentById(context.GetArgument<int>("id"));
                    if (comment == null)
                    {
                        context.Errors.Add(new ExecutionError("Comment not found"));
                        return null;
                    }
                    commentRepository.DeleteComment(comment);
                    return "Post deleted successfully";
                });

        }
    }
}
