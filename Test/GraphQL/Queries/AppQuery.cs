﻿using GraphQL;
using GraphQL.Types;
using Test.GraphQL.Types;
using Test.Models;
using Test.Repositories;

namespace Test.GraphQL.Queries
{
    public class AppQuery: ObjectGraphType
    {
        public AppQuery(IPostRepository postRepository, ICommentRepository commentRepository)
        {
            Field<ListGraphType<PostType>>(
                "posts",
                resolve: context => postRepository.GetAllPosts()
                );
            Field<PostType>(
                "post",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => postRepository.GetPostById(context.GetArgument<int>("id"))
                );
            Field<ListGraphType<CommentType>>(
                "commentsByPost",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => commentRepository.GetCommentsByPostId(context.GetArgument<int>("id"))
                );
        }
    }
}
