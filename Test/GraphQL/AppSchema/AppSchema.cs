﻿using GraphQL.Types;
using GraphQL;
using Test.GraphQL.Queries;

namespace Test.GraphQL.AppSchema
{
    public class AppSchema: Schema
    {
        public AppSchema(IServiceProvider provider): base(provider)
        {
            Query = provider.GetRequiredService<AppQuery>();
            Mutation = provider.GetRequiredService<AppMutation>();
        }
    }
}
