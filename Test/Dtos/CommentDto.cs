﻿namespace Test.Dtos
{
    public class CommentDto
    {
        public string Description { get; set; }

        public int PostId { get; set; }

    }
}
