﻿namespace Test.Dtos
{
    public class PostDto
    {
        public string Title { get; set; }

        public string Description { get; set; }

    }
}
