﻿using Test.Models;

namespace Test.Repositories
{
    public interface ICommentRepository
    {
        Task<IEnumerable<Comment>> GetAllComments();
        Comment GetCommentById(int id);
        IEnumerable<Comment> GetCommentsByPostId(int postId, int? num = null);
        Task<Comment> AddComment(Comment comment);
        Task UpdateComment(Comment comment);
        void DeleteComment(Comment comment);
    }
}
