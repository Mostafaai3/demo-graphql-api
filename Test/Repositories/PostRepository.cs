﻿using Microsoft.EntityFrameworkCore;
using Test.Models;

namespace Test.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly TestDbContext _context;
        public PostRepository(TestDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Post>> GetAllPosts()
        {
            return await _context.Posts.Include(p => p.Comments).ToListAsync();
        }

        public Post GetPostById(int id)
        {
            return _context.Posts.FirstOrDefault(p => p.Id == id);
        }

        public async Task<Post> AddPost(Post post)
        {
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            return post;
        }

        public Post UpdatePost(Post post)
        {
            _context.Entry(post).State = EntityState.Modified;
            _context.SaveChangesAsync();
            return post;
        }

        public void DeletePost(Post post)
        {
            _context.Posts.Remove(post);
            _context.SaveChanges();
        }
    }
}
