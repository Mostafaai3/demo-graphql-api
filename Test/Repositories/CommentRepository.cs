﻿using Microsoft.EntityFrameworkCore;
using Test.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.Dtos;
using Microsoft.Extensions.Hosting;

namespace Test.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly TestDbContext _context;

        public CommentRepository(TestDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Comment>> GetAllComments()
        {
            var comments = await _context.Comments.Include(c => c.Post).ToListAsync();
            return comments;
        }

        public Comment GetCommentById(int id)
        {
            return _context.Comments.FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Comment> GetCommentsByPostId(int postId, int? num)
        {
            if (num != null)
            {
                return  _context.Comments.Where(c => c.Post.Id == postId).OrderByDescending(c => c.Id).Take((int)num);
            }
            return  _context.Comments.Where(c => c.Post.Id == postId).OrderByDescending(c => c.Id);
        }


        public async Task UpdateComment(Comment comment)
        {
            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<Comment> AddComment(Comment comment)
        {
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();
            return comment;
        }

        public void DeleteComment(Comment comment)
        {
            _context.Comments.Remove(comment);
            _context.SaveChanges();
        }
    }
}