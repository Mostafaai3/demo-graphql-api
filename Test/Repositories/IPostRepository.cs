﻿using Test.Models;

namespace Test.Repositories
{
    public interface IPostRepository
    {
        Task<IEnumerable<Post>> GetAllPosts();
        Post GetPostById(int id);
        Task<Post> AddPost(Post post);
        Post UpdatePost(Post post);
        void DeletePost(Post post);
    }
}
